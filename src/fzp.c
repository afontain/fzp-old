#include <stdio.h>
#include <drawer.h>

#define HANDY_USE_UNSTABLE_API
#include <handy.h>

//FIXME
#define APP_NAME "ch.gnugen.afontain.fzp"
#define FZP_VERSION "0.0.0"

#include "phosh-wayland.h"

#include <glib/gi18n.h>
#include <glib-unix.h>


static gboolean
quit (gpointer unused)
{
  g_debug ("Cleaning up");
  gtk_main_quit ();

  return G_SOURCE_REMOVE;
}


static gboolean
on_shutdown_signal (gpointer unused)
{
  //phosh_shell_fade_out (phosh_shell_get_default (), 0);
  g_timeout_add_seconds (2, (GSourceFunc)quit, NULL);

  return FALSE;
}


static void
print_version (void)
{
  printf ("Phosh %s - A Wayland shell for mobile devices\n", FZP_VERSION);
  exit (0);
}

static void
say_hello(gpointer unused)
{
	printf("Hello, world!\n");
}

static void
say_back(gpointer string_ptr)
{
	char *string = string_ptr;
	printf("I'm saying \"%s\"\n", string);
}

static void
startup (void)
{
  GtkCssProvider *css_provider = gtk_css_provider_new ();

  gtk_css_provider_load_from_resource (css_provider, "/sm/puri/phosh/style.css"); //FIXME
  gtk_style_context_add_provider_for_screen (gdk_screen_get_default (),
                                             GTK_STYLE_PROVIDER (css_provider),
                                             GTK_STYLE_PROVIDER_PRIORITY_APPLICATION);
  g_object_unref (css_provider);
}

static void
show_drawer (void)
{
	FzpDrawer *drawer;

	char         *text[] = { "hi", "sayback something", "sayback nothing", NULL };
	fzp_callback  funs[] = { &say_hello, &say_back, &say_back, NULL };
	gpointer      args[] = { NULL,(void*) "something", (void*)"nothing", NULL };

	struct drawer_content content = {3, text, funs, args};
	drawer = fzp_drawer_new (&content);

	gtk_widget_show (GTK_WIDGET (drawer));
}


int
main(int argc, char *argv[])
{
  g_autoptr(GOptionContext) opt_context = NULL;
  GError *err = NULL;
  gboolean version = FALSE;

  const GOptionEntry options [] = {
    //{"unlocked", 'U', 0, G_OPTION_ARG_NONE, &unlocked,
    // "Don't start with screen locked", NULL},
    //{"locked", 'L', 0, G_OPTION_ARG_NONE, &locked,
    // "Start with screen locked, no matter what", NULL},
    {"version", 0, 0, G_OPTION_ARG_NONE, &version,
     "Show version information", NULL},
    { NULL, 0, 0, G_OPTION_ARG_NONE, NULL, NULL, NULL }
  };

  opt_context = g_option_context_new ("- A phone graphical shell");
  g_option_context_add_main_entries (opt_context, options, NULL);
  g_option_context_add_group (opt_context, gtk_get_option_group (TRUE));
  if (!g_option_context_parse (opt_context, &argc, &argv, &err)) {
    g_warning ("%s", err->message);
    g_clear_error (&err);
    return 1;
  }

  if (version) {
    print_version ();
  }

  gtk_init (&argc, &argv);
  hdy_init (&argc, &argv);

  g_unix_signal_add (SIGTERM, on_shutdown_signal, NULL);
  g_unix_signal_add (SIGINT, on_shutdown_signal, NULL);

  startup ();
  show_drawer ();
  gtk_main ();

  return EXIT_SUCCESS;
}
