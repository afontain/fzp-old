/*
 * Copyright (C) 2019 Purism SPC
 *
 * SPDX-License-Identifier: GPL-3.0+
 */
#pragma once

#include <layersurface.h>

G_BEGIN_DECLS

typedef void (*fzp_callback) (gpointer user_data);

struct drawer_content {
	unsigned int  element_count;
	char         **label;
	fzp_callback  *callback;
	gpointer      *user_data;
};

#define FZP_TYPE_DRAWER (fzp_drawer_get_type())
G_DECLARE_FINAL_TYPE (FzpDrawer, fzp_drawer, FZP, DRAWER, PhoshLayerSurface)

FzpDrawer *fzp_drawer_new (struct drawer_content *content);

G_END_DECLS
