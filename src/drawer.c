/*
 * Copyright (C) 2018 Purism SPC
 * SPDX-License-Identifier: GPL-3.0+
 * Author: Guido Günther <agx@sigxcpu.org>
 */

#define G_LOG_DOMAIN "phosh-notification"

#include "config.h"
#include "drawer.h"
#include "shell.h"

#define HANDY_USE_UNSTABLE_API
#include <handy.h>

/**
 * SECTION:phosh-notification
 * @short_description: A notification
 * @Title: FzpDrawer
 */

enum {
  PROP_0,
  PROP_CONTENT,
  LAST_PROP
};
static GParamSpec *props[LAST_PROP];

enum {
  SIGNAL_ACTIONED,
  N_SIGNALS
};
static guint signals[N_SIGNALS];

struct _FzpDrawer
{
  PhoshLayerSurface parent;

  GtkWidget *box_actions;

  GIcon     *icon;
  GIcon     *image;
  GAppInfo  *info;
  GStrv      actions;

  struct {
    gdouble progress;
    gint64  last_frame;
  } animation;
};

G_DEFINE_TYPE (FzpDrawer, fzp_drawer, PHOSH_TYPE_LAYER_SURFACE)


static void
fzp_drawer_set_property (GObject      *object,
                         guint         property_id,
                         const GValue *value,
                         GParamSpec   *pspec)
{
	FzpDrawer *self = FZP_DRAWER (object);

	switch (property_id) {
		case PROP_CONTENT: {
			struct drawer_content *content = g_value_get_pointer (value);
			for (unsigned int i=0; i<content->element_count; i++) {
				printf("	%s: ", content->label[i]);
				content->callback[i](content->user_data[i]);
			}
			break;
		}
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
			break;
	}
}

static void
fzp_drawer_get_property (GObject    *object,
                         guint       property_id,
                         GValue     *value,
                         GParamSpec *pspec)
{
	FzpDrawer *self = FZP_DRAWER (object);

	switch (property_id) {
		case PROP_CONTENT:
			g_value_set_pointer (value, NULL);
			break;
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
			break;
	}
}

static void
fzp_drawer_finalize (GObject *object)
{
//  FzpDrawer *self = FZP_DRAWER (object);
//
//  g_clear_object (&self->icon);
//  g_clear_object (&self->image);
//  g_clear_object (&self->info);
//  g_clear_pointer (&self->actions, g_strfreev);

  G_OBJECT_CLASS (fzp_drawer_parent_class)->finalize (object);
}


static void
fzp_drawer_slide (FzpDrawer *self)
{
	gint margin;
	gint height;
	gdouble progress = hdy_ease_out_cubic (self->animation.progress);

	progress = 1.0 - progress;

	gtk_window_get_size (GTK_WINDOW (self), NULL, &height);
	margin = (height - 300) * progress;

	phosh_layer_surface_set_margins (PHOSH_LAYER_SURFACE (self), margin, 0, 0, 0);

	phosh_layer_surface_wl_surface_commit (PHOSH_LAYER_SURFACE (self));
}


static gboolean
animate_down_cb (GtkWidget     *widget,
                 GdkFrameClock *frame_clock,
                 gpointer       user_data)
{
  gint64 time;
  gboolean finished = FALSE;
  FzpDrawer *self = FZP_DRAWER (widget);

  time = gdk_frame_clock_get_frame_time (frame_clock) - self->animation.last_frame;
  if (self->animation.last_frame < 0) {
    time = 0;
  }

  self->animation.progress += 0.06666 * time / 16666.00;
  self->animation.last_frame = gdk_frame_clock_get_frame_time (frame_clock);

  if (self->animation.progress >= 1.0) {
    finished = TRUE;
    self->animation.progress = 1.0;
  }

  fzp_drawer_slide (self);

  return finished ? G_SOURCE_REMOVE : G_SOURCE_CONTINUE;
}


static void
fzp_drawer_show (GtkWidget *widget)
{
	FzpDrawer *self = FZP_DRAWER (widget);
	gboolean enable_animations;

	enable_animations = hdy_get_enable_animations (GTK_WIDGET (self));

	self->animation.last_frame = -1;
	self->animation.progress = enable_animations ? 0.0 : 1.0;
	gtk_widget_add_tick_callback (GTK_WIDGET (self), animate_down_cb, NULL, NULL);

	GTK_WIDGET_CLASS (fzp_drawer_parent_class)->show (widget);
}


static void
fzp_drawer_class_init (FzpDrawerClass *klass)
{
  GObjectClass *object_class = (GObjectClass *)klass;
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->finalize = fzp_drawer_finalize;
  object_class->set_property = fzp_drawer_set_property;
  object_class->get_property = fzp_drawer_get_property;

  widget_class->show = fzp_drawer_show;

  props[PROP_CONTENT] =
    g_param_spec_pointer (
      "content",
      "Content",
      "A (struct drawer_content *) what's wanted",
      G_PARAM_WRITABLE | G_PARAM_CONSTRUCT /* | G_PARAM_CONSTRUCT_ONLY*/);
  g_object_class_install_properties (object_class, LAST_PROP, props);

  gtk_widget_class_set_template_from_resource (widget_class,
                                               "/sm/puri/phosh/ui/drawer.ui");
  gtk_widget_class_bind_template_child (widget_class, FzpDrawer, box_actions);

  //gtk_widget_class_bind_template_callback (widget_class, activate_notification);
  gtk_widget_class_set_css_name (widget_class, "fzf-drawer");
}

static void
action_activate (GSimpleAction *action,
                 GVariant      *parameter,
                 gpointer       data)
{
  const char *target;

  g_return_if_fail (FZP_IS_DRAWER (data));

  target = g_variant_get_string (parameter, NULL);

  g_signal_emit (data, signals[SIGNAL_ACTIONED], 0, target);
}

static GActionEntry entries[] =
{
  { "activate", action_activate, "s", NULL, NULL },
};

static void
fzp_drawer_init (FzpDrawer *self)
{
	//g_autoptr (GActionMap) map = NULL;

	//self->animation.progress = 0.0;

	//map = G_ACTION_MAP (g_simple_action_group_new ());
	//g_action_map_add_action_entries (map,
	//                                 entries,
	//                                 G_N_ELEMENTS (entries),
	//                                 self);
	//gtk_widget_insert_action_group (GTK_WIDGET (self),
	//                                "noti",
	//                                G_ACTION_GROUP (map));

	gtk_widget_init_template (GTK_WIDGET (self));
}


FzpDrawer *
fzp_drawer_new (struct drawer_content *content)
{
	PhoshWayland *wl = phosh_wayland_get_default ();
	int width = 360;
	//phosh_shell_get_usable_area (phosh_shell_get_default (),
	//                             NULL, NULL, &width, NULL);

	return g_object_new (FZP_TYPE_DRAWER,
	                     "content", (gpointer) content,
	                     "layer-shell", phosh_wayland_get_zwlr_layer_shell_v1(wl),
	                     "layer", ZWLR_LAYER_SHELL_V1_LAYER_OVERLAY,
	                     "anchor", ZWLR_LAYER_SURFACE_V1_ANCHOR_TOP,
	                     "margin-top", 300,
	                     "height", 150,
	                     "width", MIN (width, 450),
	                     "exclusive-zone", 0,
	                     "kbd-interactivity", FALSE,
	                     "namespace", "phosh notification",
	                     NULL);
}
